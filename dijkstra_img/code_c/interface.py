# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import ctypes as ctps
from os import name as OS_NAME
from pathlib import Path as path_t
from typing import Sequence

# /!\ Built from source on 2024-10-02 using:
#     STDCXX=17 MAKE_NPROCS=32 pip install --verbose cppyy --no-binary=cppyy-cling
#     (https://cppyy.readthedocs.io/en/latest/installation.html#install-from-source)
# since the Pypi version and the AUR version were crashing.
import cppyy as cpps
import numpy as nmpy

from dijkstra_img.main import path_nfo_h, site_h

folder = path_t(__file__).parent
library_path = folder / ".." / f"{OS_NAME}.so"

cpps.include(folder / "dijkstra.cpp")  # Faster to include cpp than hpp.
if library_path.exists():
    cpps.load_library(str(library_path))


site_t = cpps.gbl.dijkstra_img.site_t
sites_t = cpps.gbl.dijkstra_img.sites_t
lengths_t = cpps.gbl.dijkstra_img.lengths_t

array_t = nmpy.ndarray


AllSteps = cpps.gbl.dijkstra_img.AllSteps
ValidSteps = cpps.gbl.dijkstra_img.ValidSteps
DijkstraShortestPathInC = cpps.gbl.dijkstra_img.DijkstraShortestPath


def DijkstraShortestPath(
    costs: array_t,
    source: site_h,
    target: site_h | Sequence[site_h],
    /,
    *,
    should_constrain_steps: bool = True,
    should_return_all: bool = False,
    should_show_progress: bool = False,
) -> path_nfo_h | tuple[path_nfo_h, ...]:
    """"""
    if isinstance(target[0], int):
        targets = (target,)
    else:
        targets = target
    source = site_t(source)
    targets = sites_t(tuple(site_t(_tgt) for _tgt in targets))

    costs = costs.astype(nmpy.float64, order="F", copy=False)
    if not costs.flags.f_contiguous:
        raise RuntimeError(
            "Cost array not Fortran-contiguous; Please contact developer."
        )
    pointer = costs.ctypes.data_as(ctps.POINTER(ctps.c_double)).contents

    lengths = lengths_t(costs.shape)

    if should_constrain_steps:
        valid_steps = ValidSteps(source, targets)
    else:
        valid_steps = AllSteps(costs.ndim)

    if should_show_progress:
        print(
            f"    Tgt: {targets.__len__()}, "
            f"Costs: "
            f"{nmpy.count_nonzero(nmpy.logical_not(nmpy.isinf(costs)))}/{costs.size}, "
            f"Steps: {valid_steps.__len__()}"
        )
    paths = DijkstraShortestPathInC(
        source, targets, pointer, lengths, valid_steps, should_show_progress
    )

    all_paths = []
    min_length = nmpy.inf
    idx_of_shortest = None
    for p_idx, path in enumerate(paths):
        length = path.length
        if length < 0.0:
            all_paths.append(((), None))
        else:
            if length < min_length:
                min_length = length
                idx_of_shortest = p_idx
            all_paths.append((tuple(tuple(_ste) for _ste in path.sites), length))

    if should_return_all:
        return tuple(all_paths)

    if idx_of_shortest is None:
        return (), None

    return all_paths[idx_of_shortest]
