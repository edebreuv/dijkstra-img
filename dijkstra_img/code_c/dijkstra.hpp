// Copyright CNRS/Inria/UNS
// Contributor(s): Eric Debreuve (since 2019)
//
// eric.debreuve@cnrs.fr
//
// This software is governed by the CeCILL  license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include <vector>

extern "C" {
namespace dijkstra_img {

    typedef std::size_t index_t;
    // Signed integer to allow arithmetic. Note: in English, Z is indeed named integers.
    typedef std::ptrdiff_t integer_t;

    typedef index_t length_t; // Storage length, not path length.
    typedef std::vector<length_t> lengths_t; // List of...

    typedef integer_t coordinate_t;
    typedef std::vector<coordinate_t> site_t; // Tuple of...
    typedef std::vector<site_t> sites_t; // List of...

    typedef integer_t shift_t;
    typedef std::vector<shift_t> vector_t; // Tuple of...
    typedef std::vector<vector_t> vectors_t; // List of...

    struct path_t {
        sites_t sites;
        double length; // Negative if no path exists.
    };
    typedef std::vector<path_t> paths_t;

    vectors_t AllSteps(length_t dimension);
    vectors_t ValidSteps(site_t& source, sites_t& targets);
    paths_t DijkstraShortestPath(site_t& source, sites_t& targets, double* costs,
        lengths_t& lengths, vectors_t& valid_steps, bool should_show_progress);

} // namespace dijkstra_img
}
