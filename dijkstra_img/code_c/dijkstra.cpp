// Copyright CNRS/Inria/UNS
// Contributor(s): Eric Debreuve (since 2019)
//
// eric.debreuve@cnrs.fr
//
// This software is governed by the CeCILL  license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

// Inspired from:
// https://www.techiedelight.com/single-source-shortest-paths-dijkstras-algorithm

#include "dijkstra.hpp"
#include <algorithm> // transform
#include <cfloat> // DBL_MAX
#include <chrono>	// std::chrono::milliseconds
#include <cmath> // sqrt
#include <iostream> // cout
#include <map>
#include <numeric> // accumulate, inner_product
#include <pthread.h>
#include <queue> // priority_queue
#include <set>
#include <stdexcept> // invalid_argument
#include <thread>	//std::this_thread::sleep_for
#include <vector>

extern "C" {
namespace dijkstra_img {
    const int PROGRESS_PERIOD = 2000;

    typedef index_t linear_coord_t;
    typedef linear_coord_t (*coordinates_to_linear_t)(site_t& site, lengths_t& lengths);

    struct neighbor_t {
        site_t site;
        double step_length; // Negative if invalid or out-of-bounds.
    };
    typedef std::vector<neighbor_t> neighbors_t; // List of...

    struct site_w_distance_t { // For priority queue.
        site_t site;
        double distance; // Distance from source to self.
    };
    struct site_comparator_t { // For priority queue sorting.
        bool operator()(site_w_distance_t& lhs, site_w_distance_t& rhs)
        {
            return lhs.distance > rhs.distance;
        }
    };
    typedef std::priority_queue<site_w_distance_t, std::vector<site_w_distance_t>,
        site_comparator_t>
        priority_queue_t;

    typedef std::set<site_t> target_set_t;
    typedef std::map<site_t, site_t> predecessors_t;

    // Must be the highest possible value for priority queue.
    const double INFINITE_LENGTH = DBL_MAX;

    vectors_t AllSteps(length_t dimension)
    {
        vectors_t output;

        if (dimension == 2) {
            for (shift_t row = -1; row < 2; row++) {
                for (shift_t col = -1; col < 2; col++) {
                    if ((row != 0) || (col != 0)) {
                        vector_t step({ row, col });
                        output.push_back(step);
                    }
                }
            }
        } else if (dimension == 3) {
            for (shift_t row = -1; row < 2; row++) {
                for (shift_t col = -1; col < 2; col++) {
                    for (shift_t slice = -1; slice < 2; slice++) {
                        if ((row != 0) || (col != 0) || (slice != 0)) {
                            vector_t step({ row, col, slice });
                            output.push_back(step);
                        }
                    }
                }
            }
        } else {
            throw std::invalid_argument("Invalid dimension");
        }

        return output;
    }

    inline bool _CoherentStraightPaths(vector_t& step,
        vectors_t& straight_paths)
    {
        for (vector_t& path : straight_paths)
            if (std::inner_product(step.begin(), step.end(), path.begin(), 0) >= 0)
                return true;

        return false;
    }

    vectors_t ValidSteps(site_t& source, sites_t& targets)
    {
        vectors_t output;

        char dimension = source.size();

        vectors_t straight_paths(targets.size());
        length_t t_idx = 0;
        for (vector_t& path : straight_paths) {
            path.resize(dimension);
            std::transform(targets[t_idx].begin(), targets[t_idx].end(), source.begin(),
                path.begin(), std::minus<coordinate_t>());
            t_idx++;
        }

        if (dimension == 2) {
            for (shift_t row = -1; row < 2; row++) {
                for (shift_t col = -1; col < 2; col++) {
                    if ((row != 0) || (col != 0)) {
                        vector_t step({ row, col });
                        if (_CoherentStraightPaths(step, straight_paths)) {
                            output.push_back(step);
                        }
                    }
                }
            }
        } else if (dimension == 3) {
            for (shift_t row = -1; row < 2; row++) {
                for (shift_t col = -1; col < 2; col++) {
                    for (shift_t slice = -1; slice < 2; slice++) {
                        if ((row != 0) || (col != 0) || (slice != 0)) {
                            vector_t step({ row, col, slice });
                            if (_CoherentStraightPaths(step, straight_paths)) {
                                output.push_back(step);
                            }
                        }
                    }
                }
            }
        } else {
            throw std::invalid_argument("Invalid dimension");
        }

        return output;
    }

    inline linear_coord_t _2_D_CoordinatesToLinear(site_t& site, lengths_t& lengths)
    {
        return ((linear_coord_t)site[0]) + lengths[0] * ((linear_coord_t)site[1]);
    }

    inline linear_coord_t _3_D_CoordinatesToLinear(site_t& site, lengths_t& lengths)
    {
        return (
            (linear_coord_t)site[0]) +
            lengths[0] * (((linear_coord_t)site[1]) +
            lengths[1] * ((linear_coord_t)site[2])
        );
    }

    static void _SetNeighbors(site_t& site, neighbors_t& neighbors,
        vectors_t& valid_steps,
        double* valid_step_lengths, double* costs,
        lengths_t& lengths,
        coordinates_to_linear_t CoordinatesToLinear)
    {
        bool is_inside;

        length_t s_idx = 0;
        for (neighbor_t& neighbor : neighbors) {
            site_t& raw_neighbor = neighbor.site;
            vector_t& valid_step = valid_steps[s_idx];

            is_inside = true;
            length_t c_idx = 0;
            for (coordinate_t& coordinate : raw_neighbor) {
                coordinate = site[c_idx] + valid_step[c_idx];
                if ((coordinate < 0) || (coordinate >= (coordinate_t)lengths[c_idx])) {
                    is_inside = false;
                    neighbor.step_length = -1.0;
                    break;
                }
                c_idx++;
            }

            if (is_inside)
                // Negative if costs[...] is.
                neighbor.step_length = costs[CoordinatesToLinear(raw_neighbor, lengths)]
                    * valid_step_lengths[s_idx];

            s_idx++;
        }
    }

    static sites_t _ShortestPathToTarget(site_t& target,
        predecessors_t& predecessors)
    {
        sites_t output({ target });

        site_t site(target);
        while (predecessors.find(site) != predecessors.end()) {
            site = predecessors[site];
            output.insert(output.begin(), site);
        }

        return output;
    }

    static paths_t _ShortestPathsToTargets(sites_t& targets,
        double* distances_from_src,
        predecessors_t& predecessors,
        lengths_t& lengths,
        coordinates_to_linear_t CoordinatesToLinear)
    {
        paths_t output(targets.size());

        length_t t_idx = 0;
        path_t path;
        for (site_t& target : targets) {
            double distance = distances_from_src[CoordinatesToLinear(target, lengths)];
            if (distance != INFINITE_LENGTH) {
                sites_t sites = _ShortestPathToTarget(target, predecessors);
                path = { .sites = sites, .length = distance };
            } else {
                sites_t null_path_nodes {};
                path = { .sites = null_path_nodes, .length = -1.0 };
            }
            output[t_idx] = path;

            t_idx++;
        }

        return output;
    }

    length_t N_SITES;
    priority_queue_t PRIORITY_QUEUE;
    static void *_ShowProgress(void *raw_visited)
    {
        pthread_detach(pthread_self());

        bool* visited = (bool*) raw_visited;
        while (1) {
            int n_visited = std::count_if(
                visited,
                visited + N_SITES,
                [](bool _elm) { return _elm; }
            );
            std::cout << "    Visited: "
                << n_visited
                << " In Queue: "
                << PRIORITY_QUEUE.size()
                << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(PROGRESS_PERIOD));

            pthread_testcancel();
        }

        return NULL;
    }

    paths_t DijkstraShortestPath(site_t& source, sites_t& targets, double* costs,
        lengths_t& lengths, vectors_t& valid_steps, bool should_show_progress)
    {
        coordinates_to_linear_t CoordinatesToLinear;
        if (source.size() == 2)
            CoordinatesToLinear = _2_D_CoordinatesToLinear;
        else
            CoordinatesToLinear = _3_D_CoordinatesToLinear;

        length_t n_sites = std::accumulate(lengths.begin(), lengths.end(), 1,
            std::multiplies<length_t>());
        length_t n_valid_steps = valid_steps.size();

        double* valid_step_lengths = new double[n_valid_steps];
        length_t s_idx = 0;
        for (vector_t& step : valid_steps) {
            double sq_length = 0.0;
            for (shift_t& shift : step)
                sq_length += shift * shift;
            valid_step_lengths[s_idx] = sqrt(sq_length);

            s_idx++;
        }
        neighbors_t neighbors(n_valid_steps);
        for (length_t s_idx = 0; s_idx < n_valid_steps; s_idx++) {
            site_t neighbor(source.size());
            neighbors[s_idx].site = neighbor;
        }

        // To track sites for which shortest path has already been found
        bool* visited = new bool[n_sites];
        double* distances_from_src = new double[n_sites];
        predecessors_t predecessors; // Site predecessors along shortest path

        for (length_t s_idx = 0; s_idx < n_sites; s_idx++) {
            visited[s_idx] = false;
            distances_from_src[s_idx] = INFINITE_LENGTH;
        }
        distances_from_src[CoordinatesToLinear(source, lengths)] = 0.0;

        priority_queue_t priority_queue;
        priority_queue.push({ source, 0.0 });

        target_set_t unreached_targets(targets.begin(), targets.end());

        pthread_t thread;
        pthread_attr_t attr;
        if (should_show_progress) {
            N_SITES = n_sites;
            PRIORITY_QUEUE = priority_queue;
            pthread_attr_init(&attr);
            pthread_create(&thread, &attr, _ShowProgress, visited);
        }

        site_w_distance_t best_so_far;
        site_t current;
        double dist_to_current, dist_to_next, step_length;
        linear_coord_t linear_current, linear_next;
        do {
            best_so_far = priority_queue.top();
            priority_queue.pop();

            current = best_so_far.site;
            dist_to_current = best_so_far.distance;

            if (unreached_targets.find(current) != unreached_targets.end()) {
                unreached_targets.erase(current);
                if (unreached_targets.empty())
                    break;
                else
                    continue;
            }

            linear_current = CoordinatesToLinear(current, lengths);
            if (!visited[linear_current]) {
                visited[linear_current] = true;

                _SetNeighbors(
                    current, neighbors, valid_steps, valid_step_lengths, costs, lengths,
                    CoordinatesToLinear
                );
                for (neighbor_t& neighbor : neighbors) {
                    step_length = neighbor.step_length;
                    if (step_length >= 0.0) {
                        site_t& next = neighbor.site;
                        dist_to_next = dist_to_current + step_length;

                        linear_next = CoordinatesToLinear(next, lengths);
                        if (dist_to_next < distances_from_src[linear_next]) {
                            distances_from_src[linear_next] = dist_to_next;
                            predecessors[next] = current;
                            priority_queue.push({ next, dist_to_next });
                        }
                    }
                }
            }
        }
        while (!priority_queue.empty());

        if (should_show_progress) {
            pthread_cancel(thread);
            pthread_join(thread, NULL);
        }

        paths_t output = _ShortestPathsToTargets(
            targets, distances_from_src, predecessors, lengths, CoordinatesToLinear
        );

        delete[] valid_step_lengths;
        delete[] visited;
        delete[] distances_from_src;

        return output;
    }

} // namespace dijkstra_img
}

//    inline coordinate_t** _New2DArray(length_t& n_rows, length_t& n_cols)
//    {
//        coordinate_t** output = new coordinate_t*[n_rows];
//
//        output[0] = new coordinate_t[n_rows * n_cols];
//        for (length_t idx = 1; i < n_rows; idx++)
//            output[idx] = output[idx - 1] + n_cols;
//
//        return output;
//    }
//
//    inline void _Dispose2DArray(coordinate_t** input)
//    {
//        delete[] input[0];
//        delete[] input;
//    }

//    inline linear_coord_t _N_D_CoordinatesToLinear(site_t& site, lengths_t& lengths)
//    {
//        linear_coord_t output = (linear_coord_t)site.back();
//
//        for (integer_t idx = site.size() - 2; idx >= 0; idx--)
//            output = ((linear_coord_t)site[idx]) + lengths[idx] * output;
//
//        return output;
//    }
