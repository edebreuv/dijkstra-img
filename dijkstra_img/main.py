# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Dijkstra 1-to-n

Dijkstra Shortest Weighted Path from one image/volume pixel/voxel to one or more
image/volume pixel(s)/voxel(s).
    Graph nodes = pixels/voxels = sites.
    Graph edges = pixel/voxel neighborhood relationships.
    Edge weights = typically computed from pixel/voxel values.

Inspired from material of a course by Marc Pegon:
    https://www.ljll.math.upmc.fr/pegon/teaching.html
    https://www.ljll.math.upmc.fr/pegon/documents/BCPST/TP06_src.tar.gz
    https://www.researchgate.net/profile/Marc_Pegon
"""
import dataclasses as dtcl
import heapq as hpqe
from typing import Final, Iterator, Sequence

import numpy as nmpy
import scipy.ndimage.morphology as mphy

# Slightly slower alternative (look for SSA below)
# import scipy.ndimage as spim
# import skimage.draw as draw


site_h = tuple[int, ...]
# Actually, tuple[float, int, site_h | None], but mutability is needed.
site_nfo_h = list
path_h = tuple[site_h, ...]
path_nfo_h = tuple[path_h, float | None]

array_t = nmpy.ndarray


def DijkstraShortestPath(
    costs: array_t,
    source: site_h,
    target: site_h | Sequence[site_h],
    /,
    *,
    should_constrain_steps: bool = True,
    should_return_all: bool = False,
    should_show_progress: bool = False,
) -> path_nfo_h | tuple[path_nfo_h, ...]:
    """"""
    costs, targets, valid_steps = _ProcessedArguments(
        costs,
        source,
        target,
        should_constrain_steps,
    )
    valid_step_lengths = nmpy.linalg.norm(valid_steps, axis=1)

    if should_show_progress:
        print(
            f"    Tgt: {targets.__len__()}, "
            f"Costs: "
            f"{nmpy.count_nonzero(nmpy.logical_not(nmpy.isinf(costs)))}/{costs.size}, "
            f"Steps: {valid_steps.__len__()}"
        )

    site_queue = site_queue_t()
    site_queue.Insert(0.0, source)
    min_distance_to: dict[site_h, float] = {source: 0.0}
    predecessor_of: dict[site_h, site_h] = {}
    visited_sites: set[site_h] = set()

    # Empty queue: full graph traversal did not allow to reach all targets
    # This case is correctly dealt with in the following
    while (site_nfo := site_queue.Pop()) is not None:
        if should_show_progress:
            print(
                f"    Visited: {visited_sites.__len__()}, "
                f"In Queue: {site_queue.heap.__len__()}"
            )

        distance, current = site_nfo
        if current in targets:
            targets.remove(current)
            if targets.__len__() > 0:
                continue
            else:
                break

        if current not in visited_sites:
            visited_sites.add(current)
            for next_, step_length in _NeighborsWithCost(
                current, valid_steps, valid_step_lengths, costs
            ):
                next_as_tuple = tuple(next_)
                distance_to_next = distance + step_length
                min_so_far = min_distance_to.get(next_as_tuple)
                if (min_so_far is None) or (distance_to_next < min_so_far):
                    min_distance_to[next_as_tuple] = distance_to_next
                    predecessor_of[next_as_tuple] = current
                    site_queue.Insert(distance_to_next, next_as_tuple)

    if isinstance(target[0], int):
        targets = (target,)
    else:
        targets = target

    if should_return_all:
        return tuple(
            _PathInfoToTarget(_tgt, predecessor_of, min_distance_to) for _tgt in targets
        )

    min_distance = nmpy.inf
    closest_target = None
    for target in targets:
        distance = min_distance_to.get(target)
        if (distance is not None) and (distance < min_distance):
            min_distance = distance
            closest_target = target

    if closest_target is None:
        return (), None

    return _PathInfoToTarget(closest_target, predecessor_of, min_distance_to)


def SphereLimitedCosts(
    costs: array_t, source: site_h, target: site_h | Sequence[site_h], /
) -> array_t:
    """
    Note: Un-numba-ble because of slice
    """
    output = nmpy.full_like(costs, nmpy.inf)

    if isinstance(target[0], int):
        targets = (target,)
    else:
        targets = target

    # Keep valid, as opposed to invalid, since using invalid sites would require dealing
    # with several distance maps.
    valid_sites = nmpy.zeros_like(costs, dtype=nmpy.bool_)
    center_map = nmpy.ones_like(costs, dtype=nmpy.uint8)
    distances = nmpy.empty_like(costs, dtype=nmpy.float64)

    centers = tuple(
        tuple(nmpy.around(0.5 * nmpy.add(_tgt, source)).astype(nmpy.int64))
        for _tgt in targets
    )

    n_dims = source.__len__()

    for t_idx, target in enumerate(targets):
        sq_radius = max(
            (nmpy.subtract(centers[t_idx], source) ** 2).sum(),
            (nmpy.subtract(centers[t_idx], target) ** 2).sum(),
        )
        radius = nmpy.sqrt(sq_radius).astype(nmpy.int64) + 1
        # Note the +1 in slices ends to account for right-open ranginess
        bbox = tuple(
            slice(
                max(centers[t_idx][c_idx] - radius, 0),
                min(centers[t_idx][c_idx] + radius + 1, distances.shape[c_idx]),
            )
            for c_idx in range(n_dims)
        )
        if t_idx > 0:
            center_map[centers[t_idx - 1]] = 1
        center_map[centers[t_idx]] = 0
        local_distances = distances[bbox]
        mphy.distance_transform_edt(center_map[bbox], distances=local_distances)
        distance_thr = max(distances[source], distances[target])

        valid_sites[bbox][local_distances <= distance_thr] = True
        # # SSA
        # if n_dims == 2:
        #     valid_coords = draw.circle(
        #         *centers[t_idx], radius, shape=valid_sites.shape
        #     )
        # else:
        #     local_shape = tuple(slc.stop - slc.start for slc in bbox)
        #     local_center = tuple(centers[t_idx][c_idx] - slc.start for c_idx, slc in enumerate(bbox))
        #     valid_coords = __SphereCoords__(*local_center, radius, shape=local_shape)
        #     valid_coords = tuple(valid_coords[c_idx] + slc.start for c_idx, slc in enumerate(bbox))
        # valid_sites[valid_coords] = True

    nmpy.copyto(output, costs, where=valid_sites)

    return output


STEPS_2D: Final = nmpy.array(
    tuple((i, j) for i in (-1, 0, 1) for j in (-1, 0, 1) if (i != 0) or (j != 0)),
    dtype=nmpy.int64,
)

STEPS_3D: Final = nmpy.array(
    tuple(
        (i, j, k)
        for i in (-1, 0, 1)
        for j in (-1, 0, 1)
        for k in (-1, 0, 1)
        if (i != 0) or (j != 0) or (k != 0)
    ),
    dtype=nmpy.int64,
)

STEPS = (None, None, STEPS_2D, STEPS_3D)  # Indexed by image dimension.


def _ProcessedArguments(
    costs: array_t,
    source: site_h,
    target: site_h | Sequence[site_h],
    should_constrain_steps: bool,
    /,
) -> tuple[array_t, list[site_h], array_t]:
    """"""
    if isinstance(target[0], int):
        targets = [target]
    else:
        targets = list(target)  # Ensure it is a list, not a tuple.

    all_steps = STEPS[costs.ndim]
    if should_constrain_steps:
        valid_steps = _ValidSteps(all_steps, source, targets)
    else:
        valid_steps = all_steps

    return costs, targets, valid_steps


def _ValidSteps(
    all_steps: array_t,
    source: site_h,
    targets: Sequence[site_h],
    /,
) -> array_t:
    """"""
    shape = (source.__len__(), targets.__len__())
    straight_paths = nmpy.empty(shape, dtype=nmpy.float64)
    for t_idx, target in enumerate(targets):
        straight_paths[:, t_idx] = nmpy.subtract(target, source)

    inner_prods = all_steps @ straight_paths
    valid_steps_idc = nmpy.any(inner_prods >= 0, axis=1)

    return all_steps[valid_steps_idc, :]


def _NeighborsWithCost(
    site: site_h, steps: array_t, step_lengths: array_t, costs: array_t, /
) -> Iterator[tuple[array_t, float]]:
    """"""
    site = nmpy.array(site)[None, :]
    neighbors = site + steps
    n_dims = site.size

    inside = nmpy.all(neighbors >= 0, axis=1)
    for c_idx in range(n_dims):
        nmpy.logical_and(
            inside, neighbors[:, c_idx] <= costs.shape[c_idx] - 1, out=inside
        )
    neighbors = neighbors[inside, :]
    step_lengths = step_lengths[inside]

    # For any n_dims: neighbors_costs = costs[tuple(zip(*neighbors.tolist()))]
    if n_dims == 2:
        neighbors_costs = costs[(neighbors[:, 0], neighbors[:, 1])]
    else:
        neighbors_costs = costs[(neighbors[:, 0], neighbors[:, 1], neighbors[:, 2])]

    valid_sites = neighbors_costs >= 0
    neighbors = neighbors[valid_sites, :]
    neighbors_costs = neighbors_costs[valid_sites] * step_lengths[valid_sites]

    return zip(neighbors, neighbors_costs)


def _PathInfoToTarget(
    target: site_h,
    predecessor_of: dict[site_h, site_h],
    min_distance_to: dict[site_h, float],
    /,
) -> path_nfo_h:
    """"""
    distance = min_distance_to.get(target)
    if distance is None:
        return (), None

    path = []
    current = target
    while current is not None:
        path.append(current)
        current = predecessor_of.get(current)

    return tuple(reversed(path)), distance


@dtcl.dataclass(slots=True, repr=False, eq=False)
class site_queue_t:
    heap: list[site_nfo_h] = dtcl.field(default_factory=list)
    insertion_idx: int = 0
    visited_sites: dict[site_h, site_nfo_h] = dtcl.field(default_factory=dict)

    def Insert(self, distance: float, site: site_h, /) -> None:
        """
        Insert a new site with its distance, or update the distance of an existing site.
        """
        if site in self.visited_sites:
            self._Delete(site)
        self.insertion_idx += 1
        # Needs mutability (site: see below) => list, not tuple
        site_nfo = [distance, self.insertion_idx, site]
        self.visited_sites[site] = site_nfo
        hpqe.heappush(self.heap, site_nfo)

    def Pop(self) -> tuple[float, site_h] | None:
        """
        Return (distance, site) for the site of minimum distance, or None if queue is
        empty.
        """
        while self.heap:
            distance, _, site = hpqe.heappop(self.heap)
            if site is not None:
                del self.visited_sites[site]
                return distance, site

        return None

    def _Delete(self, site: site_h, /) -> None:
        #
        site_nfo = self.visited_sites.pop(site)
        site_nfo[-1] = None


# # SSA
# def _SphereCoords(
#     row: int, col: int, dep: int, radius: int, shape: tuple[int, int, int], /
# ) -> np_array_picker_h:
#     """"""
#     sphere = nmpy.zeros(shape, dtype=nmpy.bool_)
#     # draw.ellipsoid leaves a one pixel margin around the ellipse, hence [1:-1, 1:-1, 1:-1]
#     ellipse = draw.ellipsoid(radius, radius, radius)[1:-1, 1:-1, 1:-1]
#     sp_slices = tuple(
#         slice(0, min(sphere.shape[idx_], ellipse.shape[idx_])) for idx_ in (0, 1, 2)
#     )
#     sphere[sp_slices] = ellipse[sp_slices]
#
#     sphere = spim.shift(
#         sphere, (row - radius, col - radius, dep - radius), order=0, prefilter=False
#     )
#
#     return sphere.nonzero()
