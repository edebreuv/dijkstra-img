# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as nmpy
import skimage.draw as draw

from dijkstra_img import (
    DijkstraShortestPath,
    DijkstraShortestPathInC,
)
from how_long import RunAndTime


array_t = nmpy.ndarray


N_ROWS = 100
N_COLS = 150
N_SLICES = 200

ALL_CORNERS = tuple(
    (i, j, k)
    for i in (1, N_ROWS - 2)
    for j in (1, N_COLS - 2)
    for k in (1, N_SLICES - 2)
)


costs = nmpy.empty((N_ROWS, N_COLS, N_SLICES), dtype=nmpy.float64)
idx = 1
p_times = []
c_times = []
for source in ALL_CORNERS:
    for target in ALL_CORNERS:
        if source == target:
            continue

        print(f"{idx}/56")
        idx += 1

        line = draw.line_nd(source, target, endpoint=True)
        costs.fill(1.0)
        costs[line] = 0.1

        line_as_sites = tuple(zip(*line))
        steps = nmpy.diff(nmpy.array(line), axis=1)
        distance = nmpy.multiply(costs[line][1:], nmpy.linalg.norm(steps, axis=0)).sum()

        (p_path, p_distance), p_time = RunAndTime(
            DijkstraShortestPath,
            costs,
            source,
            target,
            should_print_e_time=False,
            should_return_e_time=True,
        )
        (c_path, c_distance), c_time = RunAndTime(
            DijkstraShortestPathInC,
            costs,
            source,
            target,
            should_print_e_time=False,
            should_return_e_time=True,
            should_show_progress=True,
        )

        p_times.append(p_time)
        c_times.append(c_time)

        if p_path != line_as_sites:
            print(f"{source} -> {target}:\n{p_path}\n!= Expected\n{line_as_sites}")
        error = abs(p_distance - distance) / distance
        if error > 1.0e-6:
            print(f"{source} -> {target}: {p_distance} != Expected:{distance}")

        if p_path != c_path:
            print(f"{source} -> {target}:\n{p_path}\n!=\n{c_path}")
        if p_distance != c_distance:
            print(f"{source} -> {target}: {p_distance} != {c_distance}")


for name, times in (("Python", p_times), ("C-Code", c_times)):
    print(f"{name}: "
          f"[{min(times)},{max(times)}]="
          f"{nmpy.mean(times)}+-{nmpy.std(times)}")
speed_up = nmpy.divide(p_times, c_times)
print(
    f"Speed-up: "
    f"[{min(speed_up)},{max(speed_up)}]="
    f"{nmpy.mean(speed_up)}+-{nmpy.std(speed_up)}"
)
