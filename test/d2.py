# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from typing import Sequence

import matplotlib.pyplot as pypl
import numpy as nmpy
import scipy.ndimage as spim
import skimage.data as data

from dijkstra_img import (
    DijkstraShortestPath,
    DijkstraShortestPathInC,
    SphereLimitedCosts,
)
from dijkstra_img.main import site_h
from how_long import RunAndTime


array_t = nmpy.ndarray


def TestWithChelsea(n_dims: int, /) -> None:
    """
    Distance=0.001667089535292472
    Source=(70, 10)
    Closest Dest=(145, 260)
    (among (145, 260), (150, 260), (155, 260))

    Distance=0.001667089535292472, Path=(70, 10), (70, 11), (70, 12).../255/...(143, 259), (144, 259), (145, 260)
    Distance=0.0016835720351496932, Path=(70, 10), (70, 11), (70, 12).../259/...(149, 258), (149, 259), (150, 260)
    Distance=0.0016890745142158871, Path=(70, 10), (70, 11), (70, 12).../259/...(154, 258), (155, 259), (155, 260)
    """
    print("--- CHELSEA ---")

    image = data.chelsea()

    if n_dims == 2:
        source, targets = (70, 10), [(145, 260), (150, 260), (155, 260)]
        costs = 1.0 / (nmpy.sum(image, axis=2) + 1.0) ** 2
    else:
        source, targets = ((70, 10, 0), [(145, 260, 2), (150, 260, 1), (155, 260, 0)])
        costs = 1.0 / (image + 1.0) ** 2

    Test(f"Chelsea[{n_dims}]", costs, source, targets, image)


def TestWithU() -> None:
    """
    Free Distance=203.97056272808084
    Constrained Distance=416568542510.7522
    """
    print("--- U ---")

    image = nmpy.zeros((256, 192), dtype=nmpy.int8)
    image[120:200, 50:70] = 1
    image[125:200, 50:80] = 1
    image[200:220, 50:120] = 1
    image[120:220, 120:140] = 1

    source, target = (120, 69), (120, 120)
    costs = 1.0 / (image + 1.0e-10)

    Test(
        "U",
        costs,
        source,
        (target,),
        image,
        straight_line=(costs[120, 69:120].sum().item(), 120 - 68),
    )


def TestWithMaze() -> None:
    """"""
    print("--- Maze ---")

    image = nmpy.random.random((256, 256))
    image = (image > image.mean()).astype(nmpy.float64)
    image = 100.0 * spim.gaussian_filter(image, 2) ** 4

    source, target = (0, 0), (255, 255)
    costs = image

    Test("Maze", costs, source, (target,), image)


def Test(
    data: str,
    costs: array_t,
    source: site_h,
    targets: Sequence[site_h],
    image_for_display: array_t,
    /,
    *,
    straight_line: tuple[float, int] = ("?", "?"),
) -> None:
    """"""
    all_results = []
    for name, Function in (
        ("Python", DijkstraShortestPath),
        ("C-code", DijkstraShortestPathInC),
    ):
        print(f"--- --- {name}")

        path, distance = RunAndTime(Function, costs, source, targets)
        all_paths = RunAndTime(
            Function, costs, source, targets, should_return_all=True
        )

        c_path, c_distance = RunAndTime(
            Function, SphereLimitedCosts(costs, source, targets), source, targets
        )
        u_path, u_distance = RunAndTime(
            Function, costs, source, targets, should_constrain_steps=False
        )
        all_results.append(
            (name, path, distance, all_paths, c_path, c_distance, u_path, u_distance)
        )

        targets_as_str = targets.__str__()[1:-1]
        if path.__len__() > 0:
            message = (
                f"Source={source}\n"
                f"Closest Target[D-C D.]={path[-1]} among {targets_as_str}\n"
                f"Straight line={straight_line[0]} for {straight_line[1]} pixels\n"
                f"Direction-Constrained Distance="
                f"{distance} for {path.__len__()} pixels\n"
                f"Sphere-Constrained Distance="
                f"{c_distance} for {c_path.__len__()} pixels\n"
                f"Free Distance={u_distance} for {u_path.__len__()} pixels"
            )
        else:
            message = (
                f"Source={source}\n"
                f"No path to: {targets_as_str}\n"
                f"Straight line={straight_line[0]} for {straight_line[1]} pixels"
            )
        print(message)

        print("All Paths[D-C D.]:")
        for one_path, one_distance in all_paths:
            if one_path.__len__() > 6:
                path_as_str = one_path[:3].__str__()[1:-1]
                path_as_str += ".../" + (one_path.__len__() - 6).__str__() + "/..."
                path_as_str += one_path[-3:].__str__()[1:-1]
            else:
                path_as_str = one_path.__str__()[1:-1]
            print(
                f"Distance={one_distance}, "
                f"Pixels={one_path.__len__()}, "
                f"Path={path_as_str}"
            )

        image_w_paths = image_for_display.copy()
        if image_w_paths.ndim == 2:
            image_w_paths = image_w_paths - nmpy.amin(image_w_paths)
            image_w_paths = (64.0 * (image_w_paths / nmpy.amax(image_w_paths))).astype(
                nmpy.uint8
            )
            for distance_, path_, value in (
                (distance, path, 58),
                (c_distance, c_path, 48),
                (u_distance, u_path, 38),
            ):
                if distance_ is not None:
                    image_w_paths[tuple(zip(*path_))] += value
            image_w_paths[source] += 28
            for target in targets:
                image_w_paths[target] += 18
        else:
            for distance_, path_, color in (
                (distance, path, (255, 0, 0)),
                (c_distance, c_path, (0, 255, 0)),
                (u_distance, u_path, (0, 0, 255)),
            ):
                if distance_ is not None:
                    for row, col, *_ in path_:
                        image_w_paths[row, col, :] = color
            image_w_paths[source] = (255, 255, 0)
            for target in targets:
                image_w_paths[target] = (0, 255, 255)
        pypl.matshow(image_w_paths)
        pypl.gca().set_title(f"--- {name} ---\n{message}", fontsize="small")

    first_results = all_results[0]
    print(f"Reference: {data}:{first_results[0]}")
    for results in all_results[1:]:
        for e_idx, (reference, current) in enumerate(zip(first_results[1:], results[1:])):
            if reference != current:
                print(
                    f"/!\\ {results[0]}:{e_idx}th element:\n{reference}\n!=\n{current}"
                )


def Main() -> None:
    """"""
    import sys as sstm

    if sstm.argv.__len__() < 2:
        which_test = "all"
    else:
        which_test = sstm.argv[1]

    if which_test in ("chelsea", "all"):
        n_dims = 2  # 2 or 3
        TestWithChelsea(n_dims)
        print("")

    if which_test in ("u", "all"):
        TestWithU()
        print("")

    if which_test in ("maze", "all"):
        TestWithMaze()

    pypl.show()


if __name__ == "__main__":
    #
    Main()
