# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import time
from typing import Any, Callable


def RunAndTime(
    Function: Callable,
    *args,
    should_print_e_time: bool = True,
    prologue: str = ">>> ",
    epilogue: str = "",
    should_return_e_time: bool = False,
    **kwargs,
) -> Any | tuple[Any, float]:
    """"""
    before = time.monotonic_ns()
    output = Function(*args, **kwargs)
    after = time.monotonic_ns()
    e_time = (after - before) / 1.0e9

    if should_print_e_time:
        print(f"{prologue}{Function.__name__}: {e_time}s{epilogue}")

    if should_return_e_time:
        return output, e_time

    return output
