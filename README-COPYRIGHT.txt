Copyright CNRS/Inria/UNS
Contributor(s): Eric Debreuve (since 2019)

eric.debreuve@cnrs.fr

This software is being developed by Eric Debreuve, a CNRS employee and member of team Morpheme.
Team Morpheme is a joint team between Inria, the CNRS, and UNS (member of UCA).
It is hosted by Inria Sophia Antipolis Méditerranée, Laboratory I3S, and Laboratory iBV.

CNRS: http://www.cnrs.fr/index.html
Team Morpheme: http://team.inria.fr/morpheme/
Inria: http://www.inria.fr/en/
Inria Sophia Antipolis Méditerranée: http://www.inria.fr/en/centre/sophia
UNS: http://unice.fr/en
UCA: http://univ-cotedazur.fr/en
I3S: http://www.i3s.unice.fr/en
iBV: http://ibv.unice.fr/EN/index.php
